# Brutal Chess

Originally developed at https://sourceforge.net/projects/brutalchess/ by [gauntalus](http://sourceforge.net/users/gauntalus), [gyrojoe](http://sourceforge.net/users/gyrojoe) and [neilpa](http://sourceforge.net/users/neilpa) and published under the GPL-2.0 license.